# Whitepaper (Draft 1.0)

InternationalDAO is a social organization with an aim at funding the class struggles of the working class throughout the world. Decentralized Autonomous Association is a computer ruled organization that is transparent and controlled by shareholders. 

Amongst other things the association acts as a international solidarity fund for workers during strikes. Shareholders can also vote for action in specific countries. 

## Table of Contents

  * [**Goal**](#goal)
  * [**Constitution**](#constitution)
  * [**Tokens**](#tokens)
  * [**Governance**](#governance)
- [**Genesis**](#genesis)
  * [DAO](#dao)
  * [Governance](#governance-1)
  * [Tokens](#tokens-1)
  * [Revenues](#revenues)
  * [Expenditures](#expenditures)
    + [Development](#development)
    + [Public relations](#public-relations)
    + [Expansion](#expansion)
    + [Market Making](#market-making)
  * [Token Pricing](#token-pricing)
- [**FAQ**](#faq)
    + [DApps have zero fees, so what’s the utility of tokens in InternationalDAO?](#dapps-have-zero-fees-so-whats-the-utility-of-tokens-in-InternationalDAO)

## Goal

The goal in InternationalDAO is to decentralize everything. It supports development for DApps that are fully open-source and require zero-fee. 

## Constitution

1. There are 21 million tokens in total that are created gradually and periodically according to a predefined function of time.

2. All of the decisions in the DAO are done based on one person one vote.

## Tokens

The tokens in the organization are limited to 21 million, issued periodically similar to Bitcoin, and distributed among the following groups who contribute to the advancement of DApps:

* Hosting providers
* Open source developers
* End users

## Governance

Governance in InternationalDAO determines the details of supported DApps and distribution of incentives among them. This process is done based on one vote per person which is only possible with [BrightID](https://www.brightid.org/).


# **Genesis**

## DAO

At genesis point the DAO will be built on Aragon because it has provided the most reliable technical infrastructure for decentralized governance. That initially makes InternationalDAO an Aragon DAO that is hosted on Ethereum.

As soon as the voting DApp for InternationalDAO are launched, it will use its own voting DApp as its technical infrastructure.

## Governance

Due to the high risk and inefficiency of decentralizing a non-existing system, Initial members of the DAO will be those who contribute to development of the ecosystem. People will replace developers by their delegates as soon as the system reaches a minimum maturity.

Minimum maturity is achieved when the system has 100K users holding minimum $100 worth of DAO tokens each.

## Tokens

The tokens will be created, hosted and transferred on Ethereum until the ledger DApp is launched.

Until the ledger DApp is launched, the tokens will be created by a crowdsale smart contract and hosted on Ethereum.

Upon the launch of ledger DApp, previously created tokens hosted on Ethereum, will be exchanged 1/1 by the ledger native tokens. 

Newly created tokens won’t be sold in crowdsale on Ethereum anymore. They will rather be distributed among developers, hosting providers, and DApp users. The details of token distribution will be determined by the DAO. 

## Revenues

At genesis point, DAO tokens will be sold to investors by a crowdsale smart contract on Ethereum. The capital gained from the crowdsale will be transferred to a DAO as its revenue. 

The crowdsale smart contract generates 50 new tokens every 10 minutes. The price is set in advance for the first stack and will go higher by 1% for each of the further stacks unless they are not sold. If the stacks are not fully sold, newly generated tokens will be saved in the smart contract and are put into crowdsale at their original price. Once the generated and saved stacks of tokens in the smart contract are sold, the 1% price rise continues.

This approach of price setting in the crowdsale results into creation of secondary markets in exchanges in which the tokens might have a lower price. Investors buy tokens directly from the smart contract only if they cannot find a better deal in extra exchanges. 

## Expenditures

At genesis point, the DAO is formed of three sectors. The revenues will be transferred to the DAO and distributed among the sectors according to DAO’s governance.

### Development

Development expenditures address core DApp development costs which are prioritized according to the road map of the project. The DAO will try to keep the open source development of the core DApps decentralized and utilize as many developers as possible so long as it does not stop the DAO from reaching its road map milestones. 

### Public relations

Public relations include content creation for different media, website management, and activities in various social networks like Reddit, Twitter, etc. 

DAO will assign a part of its revenues to crowdsource tasks of public relations and use the potential capacities and skills of all its members. 

### Expansion

A rewarding system is defined in the DAO that incentivizes certain tasks e.g. joining the ID network, referring others to the system, etc. Those who perform these task receive points accordingly and enjoy the rewards as defined in the DAO.

### Market Making

Upon the genesis point, half of the revenues form a fund to support the price of tokens in possible bear markets. 
The fund is used to place buy orders in the exchange to enable investors to easily sell their tokens at a suitable price.

## Token Pricing

InternationalDAO has a simple mechanism for token pricing during the genesis period.

The price constantly remains 1 DAI until 200K tokens are sold. During this period, InternationalDAO only sells tokens without providing the option for buying them.

Achieving the 200K goal results in 100K DAI as the fund for supporting the price.

InternationalDAO then starts using the fund for placing buy orders to buy tokens for 1 DAI each.

From then on, every 1% increase in the fund increases tokens price by 1%.

On the other hand, a 1% decrease in the fund results in a 1% decrease in the price.

The balance of the fund at each point at the time is calculated by removing all DAI that is used for buying tokens from half the DAI that is received by selling tokens.


# **FAQ**

### DApps have zero fees, so what’s the utility of tokens in InternationalDAO?

Many zero fee DApps in InternationalDAO - like decentralized Uber, Amazon, Airbnb etc. - are commercial and require financial transactions between end users, and they need to use native tokens in their payments.
